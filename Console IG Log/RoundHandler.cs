﻿using Smod2.EventHandlers;
using Smod2.Events;
using Smod2;
using Smod2.API;
using Smod2.EventSystem.Events;
using Log_IG;

namespace Console_IG_Log
{
    internal class RoundHandler : IEventHandlerIntercom, IEventHandlerTeamRespawn, IEventHandlerRoundStart, IEventHandlerRoundEnd, IEventHandlerLCZDecontaminate, IEventHandlerWarheadStartCountdown, IEventHandlerWarheadStopCountdown, IEventHandlerWarheadDetonate, IEventHandlerDecideTeamRespawnQueue
    {
        private IConfigFile cf = ConfigManager.Manager.Config;
        private ConsoleIG consoleIG;

        public RoundHandler(ConsoleIG consoleIG)
        {
            this.consoleIG = consoleIG;
        }

        public void OnDecideTeamRespawnQueue(DecideRespawnQueueEvent ev)
        {
            if (cf.GetBoolValue("lig_log_decide_team_respawn", true) == true)
            {
                string team = string.Join(", ", ev.Teams);
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage("The Team Respawn Queue is : " + team , "blue" );
                    }
                }
            }
            if (cf.GetBoolValue("lig_log_decide_team_respawn_file", false) == true)
            {
                string team = string.Join(", ", ev.Teams);
                        Log.AddToLog("The Team Respawn Queue is : " + team, Log.TypeLog.Info);
            }
        }

        public void OnDecontaminate()
        {
            if (cf.GetBoolValue("lig_log_lczdecont", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage("The LCZ Decontamination is now ON.", "red");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_lczdecont_file", false) == true)
            {
                        Log.AddToLog("The LCZ Decontamination is now ON.", Log.TypeLog.Decontamination);
            }
        }

        public void OnDetonate()
        {
            if (cf.GetBoolValue("lig_log_warhead_explode", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage("The Alpha WarHead has exploded the inside of the Facility.", "red");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_warhead_explode_file", false) == true)
            {
                        Log.AddToLog("The Alpha WarHead has exploded the inside of the Facility.", Log.TypeLog.Warhead);
            }
        }

        public void OnIntercom(PlayerIntercomEvent ev)
        {
            if (ConfigManager.Manager.Config.GetBoolValue("lig_log_intercom", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(ev.Player.Name + " is speaking at intercom", "blue");
                    }
                }
            }

            if (ConfigManager.Manager.Config.GetBoolValue("lig_log_intercom_file", false) == true)
            {
                Log.AddToLog(ev.Player.Name + " is speaking at intercom", Log.TypeLog.Info);
            }
        }

        public void OnRoundEnd(RoundEndEvent ev)
        {
            if(cf.GetBoolValue("lig_log_roundend", true) == true)
            {
                foreach(Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if(perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage("------ Round End ------");
                        p.SendConsoleMessage("Class D espaced : " + ev.Round.Stats.ClassDEscaped);
                        p.SendConsoleMessage("Class D alive : " + ev.Round.Stats.ClassDAlive + "/" + ev.Round.Stats.ClassDStart);
                        p.SendConsoleMessage("Scientist espaced : " + ev.Round.Stats.ScientistsEscaped);
                        p.SendConsoleMessage("Scientist alive : " + ev.Round.Stats.ScientistsAlive + "/" + ev.Round.Stats.ScientistsStart);
                        p.SendConsoleMessage("SCP alive" + ev.Round.Stats.SCPAlive + "/" + ev.Round.Stats.SCPStart);
                        p.SendConsoleMessage("SCP have killed " + ev.Round.Stats.SCPKills + " player(s)");
                        p.SendConsoleMessage("Grenades have killed " + ev.Round.Stats.GrenadeKills + " player(s)");
                        p.SendConsoleMessage("MTF alive : " + ev.Round.Stats.NTFAlive);
                        p.SendConsoleMessage("The Round has durated : " + ev.Round.Duration + " minutes");
                        p.SendConsoleMessage("-----------------------");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_roundend_file", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        Log.AddToLog("------ Round End ------");
                        Log.AddToLog("Class D espaced : " + ev.Round.Stats.ClassDEscaped);
                        Log.AddToLog("Class D alive : " + ev.Round.Stats.ClassDAlive + "/" + ev.Round.Stats.ClassDStart);
                        Log.AddToLog("Scientist espaced : " + ev.Round.Stats.ScientistsEscaped);
                        Log.AddToLog("Scientist alive : " + ev.Round.Stats.ScientistsAlive + "/" + ev.Round.Stats.ScientistsStart);
                        Log.AddToLog("SCP alive" + ev.Round.Stats.SCPAlive + "/" + ev.Round.Stats.SCPStart);
                        Log.AddToLog("SCP have killed " + ev.Round.Stats.SCPKills + " player(s)");
                        Log.AddToLog("Grenades have killed " + ev.Round.Stats.GrenadeKills + " player(s)");
                        Log.AddToLog("MTF alive : " + ev.Round.Stats.NTFAlive);
                        Log.AddToLog("The Round has durated : " + ev.Round.Duration + " minutes");
                        Log.AddToLog("-----------------------");
                    }
                }
            }
        }

        public void OnRoundStart(RoundStartEvent ev)
        {
            if(cf.GetBoolValue("lig_log_roundstart", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage("The round has started", "green");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_roundstart_file", false) == true)
            {
                        Log.AddToLog("------------------  New round has started  ------------------", Log.TypeLog.Info);
            }
        }

        public void OnStartCountdown(WarheadStartEvent ev)
        {
            if(cf.GetBoolValue("lig_log_warhead_start", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage( ev.Activator.TeamRole.Role.ToString() + " " + ev.Activator.Name + " has activated the Warhead ! The explosion will occur in T - " + ev.TimeLeft + " seconds", "yellow");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_warhead_start_file", false) == true)
            {
                        Log.AddToLog(ev.Activator.TeamRole.Role.ToString() + " " + ev.Activator.Name + " has activated the Warhead ! The explosion will occur in T - " + ev.TimeLeft + " seconds", Log.TypeLog.Warhead);
                   
            }
        }

        public void OnStopCountdown(WarheadStopEvent ev)
        {
            if (cf.GetBoolValue("lig_log_warhead_stop", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(ev.Activator.TeamRole.Role.ToString() + " " + ev.Activator.Name + " has canceled the Warhead", "yellow");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_warhead_stop_file", false) == true)
            {
                
                        Log.AddToLog(ev.Activator.TeamRole.Role.ToString() + " " + ev.Activator.Name + " has canceled the Warhead", Log.TypeLog.Warhead);
            }
        }

        public void OnTeamRespawn(TeamRespawnEvent ev)
        {
            if (ConfigManager.Manager.Config.GetBoolValue("lig_log_team_respawn", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if (ev.SpawnChaos == true)
                        {
                            p.SendConsoleMessage("The Chaos has joined the facility", "green");
                        }
                        else
                        {
                            p.SendConsoleMessage("The MTF is here to assurate the security of the facility", "green");
                        }
                    }
                }
            }
            if (ConfigManager.Manager.Config.GetBoolValue("lig_log_team_respawn_file", false) == true)
            {
                    
                        if (ev.SpawnChaos == true)
                        {
                            Log.AddToLog("The Chaos has joined the facility", Log.TypeLog.Respawn);
                        }
                        else
                        {
                            Log.AddToLog("The MTF is here to assurate the security of the facility", Log.TypeLog.Respawn);
                        }
            }
        }
    }
}