﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smod2;
using Smod2.Attributes;
using Smod2.API;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Globalization;
using static Console_IG_Log.IpInfoAPI;

namespace Console_IG_Log
{
    [PluginDetails(
        author = "Flo - Fan, Quelq1",
        description = "Print log in your console",
        id = "flo.lig",
        name = "Log In Game",
        version = "1.1.0",
        SmodMajor = 3,
        SmodMinor = 2,
        SmodRevision = 0
        )]
    public class ConsoleIG : Plugin
    {

        public override void OnDisable()
        {
            this.Info("Plugin disabled");
        }

        public override void OnEnable()
        {
            this.Info("Plugin Enabled");
        }

        public static List<string> hideloglist = new List<string>();

        public override void Register()
        {
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_rank", new string[] { "owner" }, Smod2.Config.SettingType.LIST, true, "Set rank able to show log"));

            // In Game

                // Use in RoundHandler.cs

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_teamrespawn", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_intercom", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_roundstart", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_roundend", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_lczdecont", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_warhead_start", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_warhead_stop", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_warhead_explode", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_decide_team_respawn", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

                // Use in ServerHandler.cs

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_connect", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_join", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_disconnect", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

                // Use in PlayerHandler.cs

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_kill", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_grenade", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_command", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_ban", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_handcuff", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_elevator", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_escape", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_door", true, Smod2.Config.SettingType.BOOL, false, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_door_secure", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_authcheck", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));



            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_pd_die", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_pd_enter", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_pd_exit", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_lure", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_contain_106", true, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            // Log file

            // Server Name for Configuration Folder

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_file_name_folder", string.Empty, Smod2.Config.SettingType.STRING, true, "Set logs folder name."));

                // Use in RoundHandler.cs

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_teamrespawn_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_intercom_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_roundstart_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_roundend_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_lczdecont_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_warhead_start_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_warhead_stop_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_warhead_explode_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_decide_team_respawn_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

                // Use in ServerHandler.cs

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_connect_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_join_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_disconnect_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

                // Use in PlayerHandler.cs

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_kill_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_grenade_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_command_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_ban_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_handcuff_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_elevator_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_escape_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_door_file", false, Smod2.Config.SettingType.BOOL, false, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_door_secure_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_authcheck_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));


            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_pd_die_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_pd_enter_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_pd_exit_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_lure_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_contain_106_file", false, Smod2.Config.SettingType.BOOL, true, "Set rank able to show log"));

            // Event Handler Declaration

            this.AddEventHandlers(new RoundHandler(this), Smod2.Events.Priority.Normal);
            this.AddEventHandlers(new ServerHandler(this), Smod2.Events.Priority.Normal);
            this.AddEventHandlers(new PlayerHandler(this), Smod2.Events.Priority.Normal);

            // Command Declaration

            this.AddCommands(new string[] { "hidelog", "hl" }, new hidelog(this));
        }

        
    }

    public static class ip
    {
        public static string GetUserCountryByIp(string ip)
        {
            IpInfo ipInfo = new IpInfo();
            try
            {
                string info = new WebClient().DownloadString("http://ipinfo.io/" + ip + "/geo");
                ipInfo = JsonConvert.DeserializeObject<IpInfo>(info);
                RegionInfo myRI1 = new RegionInfo(ipInfo.Country);
                ipInfo.Country = myRI1.CurrencyEnglishName;
            }
            catch (Exception)
            {
                ipInfo.Country = null;
            }

            return ipInfo.Country;
        }
    }

    // Check if the player has got the rank


    public static class perm
    {
        public static bool CheckPerm(Player p)
        {
            string[] ranklist = Smod2.ConfigManager.Manager.Config.GetListValue("lig_rank",new string[] { "owner" }, false);
            #region
            if(ranklist.Contains(p.GetRankName()) || ranklist.Contains(p.GetUserGroup().Name) && ConsoleIG.hideloglist.Contains(p.SteamId) == false)
                return true;
            #endregion
            return false;
        }
    }
}

