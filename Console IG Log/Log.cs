﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smod2;
using System.IO;

namespace Log_IG
{
    class Log
    {
        public enum TypeLog
        {
            Kill = 0,
            TeamKill = 1,
            Spawn = 2,
            Warhead = 3,
            Respawn = 4,
            Admin = 5,
            Info = 6,
            SCP = 7,
            Door = 8,
            Decontamination = 9,
            Null = 10,
        } 

        public static void AddToLog(string text, TypeLog type = TypeLog.Null)
        {

            string server_name = ConfigManager.Manager.Config.GetStringValue("lig_file_name_folder", string.Empty);
            string path = string.Empty;
            string FileName = DateTime.Now.Day + "-" + DateTime.Now.Month + "-" + DateTime.Now.Year + ".log";
            
            string Prefix = string.Empty;
            if(type == TypeLog.Null)
            {
                Prefix = DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + " | ";
            }
            else
            {
                Prefix = type.ToString() + " | " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + " | ";
            }

            if (Directory.Exists(FileManager.GetAppFolder()))
            {
                if(!Directory.Exists(FileManager.GetAppFolder() + Path.DirectorySeparatorChar + "Logs"))
                {
                    Directory.CreateDirectory(FileManager.GetAppFolder() + Path.DirectorySeparatorChar + "Logs");
                }

                if(server_name == string.Empty)
                {
                    server_name.Replace(" ", "_");
                    if (!Directory.Exists(FileManager.GetAppFolder() + Path.DirectorySeparatorChar + "Logs" + Path.DirectorySeparatorChar + server_name))
                    {
                        Directory.CreateDirectory(FileManager.GetAppFolder() + Path.DirectorySeparatorChar + "Logs" + Path.DirectorySeparatorChar + server_name);
                    }
                    path = FileManager.GetAppFolder() + Path.DirectorySeparatorChar + "Logs" + Path.DirectorySeparatorChar + server_name;
                }
                else
                {
                    if (!Directory.Exists(FileManager.GetAppFolder() + Path.DirectorySeparatorChar + "Logs" + Path.DirectorySeparatorChar + PluginManager.Manager.Server.Port))
                    {
                        Directory.CreateDirectory(FileManager.GetAppFolder() + Path.DirectorySeparatorChar + "Logs" + Path.DirectorySeparatorChar + PluginManager.Manager.Server.Port);
                    }
                    path = FileManager.GetAppFolder() + Path.DirectorySeparatorChar + "Logs" + Path.DirectorySeparatorChar + PluginManager.Manager.Server.Port.ToString();
                }

                using(StreamWriter sw = new StreamWriter(path + Path.DirectorySeparatorChar + FileName, true))
                {
                    sw.WriteLine(Prefix + text);
                    sw.Close();
                }
            }
        }
    }
}