﻿using Smod2;
using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.API;
using System.Linq;
using Smod2.EventSystem.Events;
using System.Text.RegularExpressions;
using Log_IG;

namespace Console_IG_Log
{
    internal class PlayerHandler : IEventHandlerPlayerDie, IEventHandlerLure, IEventHandlerThrowGrenade, IEventHandlerAdminQuery, IEventHandlerBan, IEventHandlerPocketDimensionDie, IEventHandlerPocketDimensionEnter, IEventHandlerPocketDimensionExit, IEventHandlerContain106, IEventHandlerHandcuffed, IEventHandlerElevatorUse, IEventHandlerCheckEscape, IEventHandlerDoorAccess, IEventHandlerAuthCheck
 
    {

        private IConfigFile cf = ConfigManager.Manager.Config;
        private ConsoleIG consoleIG;
        private Server server = PluginManager.Manager.Server;

        public PlayerHandler(ConsoleIG consoleIG)
        {
            this.consoleIG = consoleIG;
        }

        public void OnAdminQuery(AdminQueryEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_command", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if(ev.Query != "REQUEST_DATA PLAYER_LIST SILENT")
                        {
                            p.SendConsoleMessage(ev.Admin.GetUserGroup().Name + " " + ev.Admin.Name + " has executed : " + ev.Query, "yellow");
                        }
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_player_command_file", false) == true)
            {
               
                        if (ev.Query != "REQUEST_DATA PLAYER_LIST SILENT")
                        {
                            Log.AddToLog(ev.Admin.GetUserGroup().Name + " " + ev.Admin.Name + " has executed : " + ev.Query, Log.TypeLog.Admin);
                            
                        }
            }
        }

        public void OnAuthCheck(AuthCheckEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_authcheck", true) == true)
            {
                foreach(Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if(perm.CheckPerm(p) == true)
                    {
                        if (ev.Allow == false)
                        {
                            p.SendConsoleMessage(ev.Requester.Name + " has tried to connect to the RA Panel.", "red");

                        }
                        else
                        {
                            p.SendConsoleMessage(ev.Requester.GetUserGroup().Name + " " + ev.Requester.Name + " is now connected to the RA pannel.", "orange");
                        }
                       
                    }
                    
                }
            }

            if (cf.GetBoolValue("lig_log_player_authcheck_file", false) == true)
            {
                        if (ev.Allow == false)
                        {
                            Log.AddToLog(ev.Requester.Name + " has tried to connect to the RA Panel.");

                        }
                        else
                        {
                            Log.AddToLog(ev.Requester.GetUserGroup().Name + " " + ev.Requester.Name + " is now connected to the RA pannel.", Log.TypeLog.Admin);
                        }
            }
        }
        public void OnBan(BanEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_ban", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if(ev.Reason.Length > 0)
                        {
                            p.SendConsoleMessage(ev.Player.Name + " was banned by " + ev.Admin.Name + " for " + ev.Reason + " during " + ev.Duration + " minutes.", "red");
                        }
                        else
                        {
                            p.SendConsoleMessage(ev.Player.Name + " was banned by " + ev.Admin.Name + " during " + ev.Duration + " minutes.", "red");
                        }
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_player_ban_file", false) == true)
            {
                        if (ev.Reason.Length > 0)
                        {
                            Log.AddToLog(ev.Player.Name + " was banned by " + ev.Admin.Name + " for " + ev.Reason + " during " + ev.Duration + " minutes.", Log.TypeLog.Admin);
                        }
                        else
                        {
                            Log.AddToLog(ev.Player.Name + " was banned by " + ev.Admin.Name + " during " + ev.Duration + " minutes.", Log.TypeLog.Admin);
                        }
            }
        }

        public void OnCheckEscape(PlayerCheckEscapeEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_escape", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if(ev.AllowEscape == true)
                        {
                            p.SendConsoleMessage(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has escaped from the Facility", "purple");
                        }
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_player_escape_file", false) == true)
            {
                        if (ev.AllowEscape == true)
                        {
                            Log.AddToLog(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has escaped from the Facility", Log.TypeLog.Info);
                        }
            }
        }

        public void OnContain106(PlayerContain106Event ev)
        {
            if (cf.GetBoolValue("lig_log_contain_106", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(ev.Player.Name + " has contained 106 successfully", "yellow");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_contain_106_file", false) == true)
            {
                Log.AddToLog(ev.Player.Name + " has contained 106 successfully", Log.TypeLog.SCP);
            }
        }

        public void OnDoorAccess(PlayerDoorAccessEvent ev)
        {
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if (cf.GetBoolValue("lig_log_player_door", false) == true)
                        {
                            if (ev.Allow == true && ev.Door.Name.Length <= 0)
                            {
                                p.SendConsoleMessage(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has opened a door", "blue");
                            }
                            else if (ev.Allow == false && ev.Door.Name.Length <= 0)
                            {
                                p.SendConsoleMessage(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has tried to open a door", "blue");
                            }
                        }
                        if (cf.GetBoolValue("lig_log_player_secure_door", true) == true)
                        {
                            if (ev.Allow == true && ev.Door.Name.Length > 0)
                            {
                                p.SendConsoleMessage(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has opened the door " + ev.Door.Name, "blue");
                            }
                            else if (ev.Allow == false && ev.Door.Name.Length > 0)
                            {
                                p.SendConsoleMessage(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has tried to open the door " + ev.Door.Name, "blue");
                            }
                        }
                            
                    }
                }

                if (cf.GetBoolValue("lig_log_player_door_file", false) == true)
                {
                    if (ev.Allow == true && ev.Door.Name.Length <= 0)
                    {
                        Log.AddToLog(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has opened a door", Log.TypeLog.Door);
                    }
                    else if (ev.Allow == false && ev.Door.Name.Length <= 0)
                    {
                        Log.AddToLog(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has tried to open a door", Log.TypeLog.Door);
                    }
                }
                if (cf.GetBoolValue("lig_log_player_secure_door_file", false) == true)
                {
                    if (ev.Allow == true && ev.Door.Name.Length > 0)
                    {
                        Log.AddToLog(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has opened the door " + ev.Door.Name, Log.TypeLog.Door);
                    }
                    else if (ev.Allow == false && ev.Door.Name.Length > 0)
                    {
                        Log.AddToLog(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has tried to open the door " + ev.Door.Name, Log.TypeLog.Door);
                    }
                }
            }
        }

        public void OnElevatorUse(PlayerElevatorUseEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_elevator", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has used an elevator.", "green");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_player_elevator_file", false) == true)
            {
                        Log.AddToLog(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has used an elevator.", Log.TypeLog.Info);
            }
        }

        public void OnHandcuffed(PlayerHandcuffedEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_handcuff", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        if(ev.Handcuffed == true)
                        {
                            p.SendConsoleMessage(ev.Owner.TeamRole.Role + " " + ev.Owner.Name + " has handcuffed " + ev.Player.TeamRole.Role + " " + ev.Player.Name, "green");
                        }
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_player_handcuff_file", false) == true)
            {
               
                        if (ev.Handcuffed == true)
                        {
                            Log.AddToLog(ev.Owner.TeamRole.Role + " " + ev.Owner.Name + " has handcuffed " + ev.Player.TeamRole.Role + " " + ev.Player.Name, Log.TypeLog.Info);
                        }

            }
        }

        public void OnLure(PlayerLureEvent ev)
        {
            if (cf.GetBoolValue("lig_log_lure", true) == true)
            {
                foreach(Player p in server.GetPlayers())
                {
                    if(perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(ev.Player.Name + " has broken his Femur to recontain SCP 106.", "yellow");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_lure_file", false) == true)
            {
                        Log.AddToLog(ev.Player.Name + " has broken his Femur to recontain SCP 106.", Log.TypeLog.SCP);
            }
        }

        public void OnPlayerDie(PlayerDeathEvent ev)
        {
            int[] teamNTF = { (int)Team.MTF, (int)Team.RSC }, teamChaos = { (int)Team.CHI, (int)Team.CDP };

            Player v = ev.Player;
            Player k = ev.Killer;

            if (v.Name == "Server" || k.Name == "Server" || v.Name == string.Empty || k.Name == string.Empty) { ev.SpawnRagdoll = false; return; }
            bool checkteamkill() {
                if (teamNTF.Contains((int)v.TeamRole.Team) && teamNTF.Contains((int)k.TeamRole.Team))
                    return true;
                if (teamChaos.Contains((int)v.TeamRole.Team) && teamChaos.Contains((int)k.TeamRole.Team))
                    return true;
                return false;
            }

            
            foreach (Player p in PluginManager.Manager.Server.GetPlayers())
            {
                if(perm.CheckPerm(p) == true)
                {
                    if (v.Name == "Server" || k.Name == "Server" || v.Name == string.Empty || k.Name == string.Empty) { ev.SpawnRagdoll = false; return; }
                    if (cf.GetBoolValue("lig_log_player_kill", true) == true)
                    {
                        if(checkteamkill() == true)
                        {
                            p.SendConsoleMessage(k.TeamRole.Role + " " + k.Name + " has teamkilled " + v.TeamRole.Role + " " + v.Name + " using " + ev.Killer.GetCurrentItem().ToString(), "red");
                        }
                        else if(!(ev.Player.SteamId == ev.Killer.SteamId))
                        {
                            p.SendConsoleMessage(k.TeamRole.Role + " " + k.Name + " has killed " + v.TeamRole.Role + " " + v.Name + " using " + ev.Killer.GetCurrentItem().ToString());
                        }
                        else if (k.SteamId == v.SteamId)
                        {
                            p.SendConsoleMessage(v.TeamRole.Role + " " + v.Name + " has suicided himself", "green");
                        }
                    }
                }
                
            }

            if (cf.GetBoolValue("lig_log_player_kill_file", false) == true)
            {
                if (checkteamkill() == true)
                {
                    Log.AddToLog(k.TeamRole.Role + " " + k.Name + " has teamkilled " + v.TeamRole.Role + " " + v.Name + " using " + ev.Killer.GetCurrentItem().ToString(), Log.TypeLog.TeamKill);
                }
                else if (!(ev.Player.SteamId == ev.Killer.SteamId))
                {
                    Log.AddToLog(k.TeamRole.Role + " " + k.Name + " has killed " + v.TeamRole.Role + " " + v.Name + " using " + ev.Killer.GetCurrentItem().ToString(), Log.TypeLog.Kill);
                }
                else if (k.SteamId == v.SteamId)
                {
                    Log.AddToLog(v.TeamRole.Role + " " + v.Name + " has suicided himself", Log.TypeLog.Kill);
                }
            }
        }

        public void OnPocketDimensionDie(PlayerPocketDimensionDieEvent ev)
        {
            if(cf.GetBoolValue("lig_log_pd_die", true) == true)
            {
                foreach(Player p in server.GetPlayers())
                {
                    if(perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(ev.Player.Name + " has died in the Pocket Dimension.", "green");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_pd_die_file", false) == true)
            {
                        Log.AddToLog(ev.Player.Name + " has died in the Pocket Dimension.", Log.TypeLog.SCP);
            }
        }

        public void OnPocketDimensionEnter(PlayerPocketDimensionEnterEvent ev)
        {
            if (cf.GetBoolValue("lig_log_pd_enter", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(ev.Player.Name + " has entered in the Pocket Dimension.", "green");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_pd_enter_file", false) == true)
            {
                        Log.AddToLog(ev.Player.Name + " has entered in the Pocket Dimension.", Log.TypeLog.SCP);
                    
            }
        }

        public void OnPocketDimensionExit(PlayerPocketDimensionExitEvent ev)
        {
            if (cf.GetBoolValue("lig_log_pd_exit", true) == true)
            {
                foreach (Player p in server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(ev.Player.Name + " has exited in the Pocket Dimension successfully.", "green");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_pd_exit_file", false) == true)
            {
                       Log.AddToLog(ev.Player.Name + " has exited in the Pocket Dimension successfully.", Log.TypeLog.SCP);
            }
        }

        public void OnThrowGrenade(PlayerThrowGrenadeEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_grenade", true) == true)
            {
                foreach(Player p in server.GetPlayers())
                {
                    if(ev.GrenadeType == ItemType.FLASHBANG)
                    {
                        p.SendConsoleMessage(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has thrown a FlashBang grenade", "yellow");
                    }
                    else
                    {
                        p.SendConsoleMessage(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has thrown a Frag' grenade", "yellow");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_player_grenade_file", false) == true)
            {
                    if (ev.GrenadeType == ItemType.FLASHBANG)
                    {
                        Log.AddToLog(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has thrown a FlashBang grenade", Log.TypeLog.Info);
                    }
                    else
                    {
                        Log.AddToLog(ev.Player.TeamRole.Role + " " + ev.Player.Name + " has thrown a Frag' grenade", Log.TypeLog.Info);
                    }
            }
        }
    }
}