﻿using Smod2;
using Smod2.EventHandlers;
using Smod2.API;
using Smod2.Events;
using Log_IG;

namespace Console_IG_Log
{
    internal class ServerHandler : IEventHandlerPlayerJoin, IEventHandlerConnect, IEventHandlerDisconnect
    {
        private IConfigFile cf = ConfigManager.Manager.Config;
        private ConsoleIG consoleIG;

        public ServerHandler(ConsoleIG consoleIG)
        {
            this.consoleIG = consoleIG;
        }

        public void OnConnect(ConnectEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_connect", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {

                        p.SendConsoleMessage("A player is attempting to connect...", "green");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_player_connect_file", false) == true)
            {
                        Log.AddToLog("A player is attempting to connect...", Log.TypeLog.Info);
            }
        }

        public void OnDisconnect(DisconnectEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_disconnect", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage("A player has disconnected.", "yellow");
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_player_disconnect_file", false) == true)
            {
                        Log.AddToLog("A player has disconnected.", Log.TypeLog.Info);
            }
        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_join", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string ip = ev.Player.IpAddress;
                        string trim = "::ffff:";
                        string ip2 = ip.TrimStart(trim.ToCharArray());
                        p.SendConsoleMessage(ev.Player.Name + " (" + ev.Player.SteamId + ") is now connected to the server." + "\n The IP adress is coming from " + Console_IG_Log.ip.GetUserCountryByIp(ip2), "green");
                           
                    }
                }
            }

            if (cf.GetBoolValue("lig_log_player_join_file", false) == true)
            {
                string ip = ev.Player.IpAddress;
                string trim = "::ffff:";
                string ip2 = ip.TrimStart(trim.ToCharArray());
                Log.AddToLog(ev.Player.Name + " (" + ev.Player.SteamId + ") is now connected to the server." + "\n The IP adress is coming from " + Console_IG_Log.ip.GetUserCountryByIp(ip2), Log.TypeLog.Info);
            }
        }
    }
}