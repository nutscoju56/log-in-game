# Log In Game

The plugin use Smod2 Framework. 
A simple plugin to print log in game for your moderation team !

## Installation
Extract the archive in sm_plugins.

### How to find the logs files ?
- On Windows : `%appdata%\SCP Secret Laboratory\Logs\[Server Port]` 
- On Linux : `/home/[USER]/SCP Secret Laboratory /[Server Port or lig_file_name_folder config]`

## Commands

To hide the log in the console, execute the command `hidelog` or `hl`.

## Config : In Game

**Type Info:**

-   `List`: A list with items separated by ",", for example: `list: 1,2,3,4,5`
-   `Bool`: True or False value


### Utility

| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_rank`     | List | owner | Set rank able to show log

### Other

| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_log_teamrespawn` | Bool | true | Print log relative to TeamRespawn
| `lig_log_intercom` | Bool | true | Print log relative to Intercom
| `lig_log_roundstart` | Bool | true | Print log relative to RoundStart
| `lig_log_roundend` | Bool | true | Print log relative to RoundEnd
| `lig_log_lczdecont` | Bool | true | Print log relative to LCZ Decontamination
| `lig_log_decide_team_respawn` | Bool | true | Print log relative to the Decide Team Respawn Queue

### WarHead
| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_log_warhead_start` | Bool | true | Print log when the WarHead's countdown is start
| `lig_log_warhead_stop` | Bool | true | Print log when the WarHead's countdown is stop
| `lig_log_warhead_explode` | Bool | true | Print log when the WarHead explode

### Player

| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_log_player_connect` | Bool | true | Print log when a player is connecting
| `lig_log_player_join` | Bool | true | Print log when a player join the server
| `lig_log_player_disconnect` | Bool | true | Print log when a player leave the server
| `lig_log_player_kill` | Bool | true | Print log when a player die
| `lig_log_player_grenade` | Bool | true | Print log when a player threw a grenade
| `lig_log_player_command` | Bool | true | Print log when an admin execute a command
| `lig_log_player_ban` | Bool | true | Print log when someone is ban
| `lig_log_player_handcuff` | Bool | true | Print log when a player handcuff someone
| `lig_log_player_elevator` | Bool | true | Print log when a player use an elevator
| `lig_log_player_escape` | Bool | true | Print log when a player escape from the facility
| `lig_log_player_door` | Bool | false | Print log when a player open a door
| `lig_log_player_secure_door` | Bool | true | Print log when a player open a door requiring a keycard
| `lig_log_player_authcheck` | Bool | true | Print log when a player connect to the RA Panel

### Pocket Dimension

| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_log_pd_enter` | Bool | true | Print log when someone enter to the Pocket dimension
| `lig_log_pd_exit` | Bool | true | Print log when someone exit from the Pocket dimension
| `lig_log_pd_die` | Bool | true | Print log when someone die inside the Pocket dimension

### 106 Containment
| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_log_lure` | Bool | true | Print log when someone enter to the Femur Breaker
| `lig_log_contain_106` | Bool | true | Print log when someone contain SCP 106 with the Femur Breaker

## Config : Log File System 

-   `Bool`: True or False value
-   `String` : A text value

| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
|`lig_file_name_folder`| String | Server Port | Set logs folder name.

### Other

| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_log_teamrespawn_file` | Bool | false | Print log relative to TeamRespawn
| `lig_log_intercom_file` | Bool | false | Print log relative to Intercom
| `lig_log_roundstart_file` | Bool | false | Print log relative to RoundStart
| `lig_log_roundend_file` | Bool | false | Print log relative to RoundEnd
| `lig_log_lczdecont_file` | Bool | false | Print log relative to LCZ Decontamination
| `lig_log_decide_team_respawn_file` | Bool | false | Print log relative to the Decide Team Respawn Queue

### WarHead
| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_log_warhead_start_file` | Bool | false | Print log when the WarHead's countdown is start
| `lig_log_warhead_stop_file` | Bool | false | Print log when the WarHead's countdown is stop
| `lig_log_warhead_explode_file` | Bool | false | Print log when the WarHead explode

### Player

| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_log_player_connect_file` | Bool | false | Print log when a player is connecting
| `lig_log_player_join_file` | Bool | false | Print log when a player join the server
| `lig_log_player_disconnect_file` | Bool | false | Print log when a player leave the server
| `lig_log_player_kill_file` | Bool | false | Print log when a player die
| `lig_log_player_grenade_file` | Bool | false | Print log when a player threw a grenade
| `lig_log_player_command_file` | Bool | false | Print log when an admin execute a command
| `lig_log_player_ban_file` | Bool | false | Print log when someone is ban
| `lig_log_player_handcuff_file` | Bool | false | Print log when a player handcuff someone
| `lig_log_player_elevator_file` | Bool | false | Print log when a player use an elevator
| `lig_log_player_escape_file` | Bool | false | Print log when a player escape from the facility
| `lig_log_player_door_file` | Bool | false | Print log when a player open a door
| `lig_log_player_secure_door_file` | Bool | false | Print log when a player open a door requiring a keycard
| `lig_log_player_authcheck_file` | Bool | false | Print log when a player connect to the RA Panel

### Pocket Dimension

| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_log_pd_enter_file` | Bool | false | Print log when someone enter to the Pocket dimension
| `lig_log_pd_exit_file` | Bool | false | Print log when someone exit from the Pocket dimension
| `lig_log_pd_die_file` | Bool | false | Print log when someone die inside the Pocket dimension

### 106 Containment
| Config Option      |     Value Type    |   Default Value | Description
| ------------- | ------------- | --------- | -------- |
| `lig_log_lure_file` | Bool | false | Print log when someone enter to the Femur Breaker
| `lig_log_contain_106_file` | Bool | false | Print log when someone contain SCP 106 with the Femur Breaker
